﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Mime;

namespace Pdf2Excel
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();
            string folder = System.IO.Directory.GetCurrentDirectory();
            string commandLine;
            string fileName;
            int sayac = 0;
            Console.WriteLine("Pdf To Excel Dönüştürücü (by Burak ŞEKERCİOĞLU - www.sekercioglu.eu) ");
            Console.WriteLine("*********************************************************************");
            Console.WriteLine("");
     
            foreach (string txtName in Directory.GetFiles(folder, "*.pdf"))

            {
               sayac++;
                fileName = Path.GetFileNameWithoutExtension(txtName);
            Console.WriteLine("İşleniyor.... " + fileName);

                commandLine = string.Format(@"""{1}\{0}.pdf"" /lang english /out ""{1}\{0}.xlsx"" /quit", fileName, folder);
             //   Console.WriteLine("İşleniyor.... " + commandLine);


                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo
                {
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal,
                    FileName = @"C:\Program Files (x86)\ABBYY FineReader 12\finecmd",
                    Arguments = commandLine
                };
                process.StartInfo = startInfo;
                process.Start();

                

            }
            Console.WriteLine("****************************************");
            Console.WriteLine(sayac.ToString() + " adet dosya dönüşümü yapıldı");
            Console.WriteLine("****************************************");
            Console.WriteLine("İletişim: burak@sekercioglu.eu");
            
            Console.ReadLine();

        }

    }
}
