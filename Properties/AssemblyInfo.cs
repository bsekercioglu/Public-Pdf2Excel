﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Pdf2Excel OCR Dönüştürücü")]
[assembly: AssemblyDescription("Burak ŞEKERCİOĞLU (burak@sekercioglu.eu)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("www.sekercioglu.eu")]
[assembly: AssemblyProduct("Pdf2Excel Dönüştürücü")]
[assembly: AssemblyCopyright("Copyright ©  2016 - Burak ŞEKERCİOĞLU")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c5b462b7-4c7c-429a-823e-2e9f2c05ce98")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
